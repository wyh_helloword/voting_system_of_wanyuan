function getVid(obj) {
    var vid = obj;
    layui.define(['table', 'layer', 'form'], function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
        var $ = layui.$
            , table = layui.table
            , layer = layui.layer
            , form = layui.form;
        var tableIns = table.render({
            elem: '#userList'
            ,url: '/user/' + vid //数据接口
            ,method: 'GET'
            ,page: true //开启分页
            ,toolbar: '#toolBar'
            ,defaultToolbar: ['filter', 'print']
            ,cols: [[ //表头
                {field: 'id', title: 'ID', minWidth:80, align: "center", sort: true, fixed: 'left', templet: function (d) {
                        return d.LAY_INDEX;
                    }}
                ,{field: 'jobNumber', title: '工号', align: "center", minWidth: 172, sort: true}
                ,{field: 'username', title: '姓名', align: "center", minWidth: 105}
                ,{field: 'sex', title: '性别', align: "center", width: 83, sort: true, templet: '#userSex'}
                ,{field: 'academy', title: '学院', minWidth: 90, align: "center"}
                ,{field: 'role', title: '角色', minWidth: 90, align: "center", sort: true, templet: '#userStatus'}
                ,{title: '操作', align: "center", fixed: 'right', minWidth: 100, toolbar: '#listToolBar'}
            ]]
            ,request: {
                pageName: 'pageNo'
                ,limitName: 'pageSize'
            }
            ,response: {
                statusCode: 200
            }
            ,parseData: function (res) {
                return {
                    "data": res.data.records,
                    'code': res.status,
                    'msg': res.msg,
                    "count": res.data.total
                }
            }
            ,limit: 10
            ,limits: [10, 15, 20, 25, 30]
        });

        //监听搜索
        form.on('submit(search)', function(data){
            var field = data.field;
            tableIns.reload({
                url: '/user/search/' + vid
                ,where: {
                    'username': field.username,
                    'jobNumber': field.jobNumber,
                    'academy': field.academy,
                    'sex': field.sex
                }
            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });

        //每行工具栏监听
        table.on('tool(user-list)', function (obj) {
            var data = obj.data;
            if (obj.event === 'add') {
                $.ajax({
                    url: '/candidate',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({uid: data.id, vid: vid}),
                    success: function (d) {
                        if (d.status == 200){
                            layer.msg(d.msg, {time: 1500});
                            parent.layui.table.reload('candidateList',{page: {curr: 1}}); //重载表格
                            obj.del(); //添加成功，就在该列表删除
                        }
                        else
                            layer.msg(d.msg, {icon: 2})
                    }
                })
            }
        });
        //输出接口
        exports('addCandidate', {});
    });
}