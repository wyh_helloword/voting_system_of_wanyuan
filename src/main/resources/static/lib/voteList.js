layui.define(['table', 'layer', 'form'], function (exports) { //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
    var $ = layui.$
        , table = layui.table
        , layer = layui.layer
        , form = layui.form;
    var tableIns = table.render({
        elem: '#voteList'
        , url: '/vote' //数据接口
        , method: 'GET'
        , page: true //开启分页
        , toolbar: '#toolBar'
        , defaultToolbar: ['filter', 'print']
        , cols: [[ //表头
            {field: 'id', title: 'ID', width: 70, align: "center", sort: true, fixed: 'left', templet: function (d) {
                    return d.LAY_INDEX;
                }}
            , {field: 'voteName', title: '投票名称', align: "center", minWidth: 105}
            , {field: 'setTime', title: '创建时间', align: "center", minWidth: 172, sort: true}
            , {field: 'voteLimit', title: '票数限制', width: 90, align: "center"}
            , {field: 'voteForOne', title: '同一人最多票数', width: 130, align: "center"}
            , {field: 'startTime', title: '开始时间', align: "center", minWidth: 172, sort: true}
            , {field: 'endTime', title: '结束时间', align: "center", minWidth: 172, sort: true}
            , {field: 'details', title: '简介', align: "center", minWidth: 88}
            , {field: 'isSelfVoted', title: '候选人投票', align: "center", width: 111, templet: '#voteStatus'}
            , {title: '操作', align: "center", fixed: 'right', width: 450, toolbar: '#listToolBar'}
        ]]
        , request: {
            pageName: 'pageNo'
            , limitName: 'pageSize'
        }
        , response: {
            statusCode: 200
        }
        , parseData: function (res) {
            return {
                "data": res.data.records,
                'code': res.status,
                'msg': res.msg,
                "count": res.data.total
            }
        }
        , height: 'full-200'
        , limit: 10
        , limits: [10, 15, 20, 25, 30]
    });

    //监听总工具栏
    $('#add').click(function () {
        layer.open({
            type: 2,
            title: '添加投票事件',
            shadeClose: true,
            shade: 0.8,
            area: ['700px', '600px'],
            content: '/vote/add', //iframe的url
        });
    });

    //每行工具栏监听
    table.on('tool(vote-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除？', function (index) {
                $.ajax({
                    url: '/vote/' + data.id
                    , type: 'DELETE'
                    , success: function (data) {
                        if (data.status === 200) {
                            layer.msg(data.msg);
                            obj.del();
                        } else {
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 2000
                            })
                        }
                    }
                });
                layer.close(index);
            })
        } else if (obj.event === 'update') {
            layer.open({
                type: 2
                , title: '修改投票事件'
                , content: '/vote/edit'
                , maxmin: true
                , btn: ['确定', '取消']
                , area: ['700px', '600px']
                , success: function (layero, index) {
                    var othis = layero.find('iframe').contents().find("#editForm").click();

                    othis.find('input[name="id"]').val(data.id);
                    othis.find('input[name="voteName"]').val(data.voteName);
                    othis.find('input[name="voteLimit"]').val(data.voteLimit);
                    othis.find('input[name="voteForOne"]').val(data.voteForOne);
                    if (data.isSelfVoted == true || data.isSelfVoted == 1) {
                        console.log("enter voteList");
                        console.log(othis.find('input[name="isSelfVoted"]').attr('checked', 'checked'));
                    }
                    othis.find('input[name="isSelfVoted"]').val(data.isSelfVoted);
                    othis.find('input[name="startTime"]').val(data.startTime);
                    othis.find('input[name="endTime"]').val(data.endTime);
                    othis.find('textarea[name="details"]').val(data.details);

                    form.render('checkbox');
                }
                , yes: function (index, layero) {
                    var iframeWindow = window['layui-layer-iframe' + index]
                        , submitID = 'vote-edit-submit'
                        , submit = layero.find('iframe').contents().find("#" + submitID);

                    // 监听提交
                    iframeWindow.layui.form.on('submit(' + submitID + ')', function (data1) {
                        var field = data1.field;
                        // 提交 Ajax 成功后，静态更新表格中的数据

                        //表单验证
                        var startTime = $('#start_time').val();
                        var endTime = $('#end_time').val();

                        if (startTime >= endTime) {
                            layer.alert('结束时间不能早于开始时间', {icon: 2});
                            return false;
                        }

                        $.ajax({
                            url: '/vote',
                            type: 'PUT',
                            contentType: "application/json",
                            data: JSON.stringify(field),
                            success: function (res) {
                                if (res.status == 200) {
                                    obj.update({
                                        voteName: field.voteName
                                        , voteLimit: field.voteLimit
                                        , voteForOne: field.voteForOne
                                        , isSelfVoted: field.isSelfVoted
                                        , startTime: field.startTime
                                        , endTime: field.endTime
                                        , details: field.details
                                    }); // 数据更新
                                    layer.msg(res.msg);
                                    form.render();
                                    // 关闭弹层
                                } else
                                    layer.msg(res.msg);
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            })
        } else if (obj.event === 'download') {
            window.open('/qr/' + data.id);
        } else if (obj.event === 'switch') {
            $.ajax({
                url: '/vote',
                type: 'PUT',
                contentType: "application/json",
                data: JSON.stringify({id: data.id, isSelfVoted: !data.isSelfVoted}),
                success: function (d) {
                    layer.msg(d.msg);
                    // 重载表格
                    form.render();
                }
            })
        } else if (obj.event === 'check'){
            layer.open({
                type: 2,
                title: '投票情况',
                shadeClose: true,
                shade: 0.8,
                area: ['90%', '90%'],
                content: '/candidate/list',
                success: function (layero, index) {
                    var iframe = window['layui-layer-iframe' + index];
                    iframe.child(data.id);
                }
            });
        } else if (obj.event === 'excel'){
            window.open("/data/" + data.id);
        }
    });
    //输出接口
    exports('voteList', {});
});