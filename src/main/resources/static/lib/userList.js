//表格渲染
layui.define(['table', 'layer', 'form'], function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
    var $ = layui.$
        , table = layui.table
        , layer = layui.layer
        , form = layui.form;
    var tableIns = table.render({
        elem: '#userList'
        ,url: '/user' //数据接口
        ,method: 'GET'
        ,page: true //开启分页
        ,toolbar: '#toolBar'
        ,defaultToolbar: ['filter', 'print']
        ,cols: [[ //表头
            {field: 'id', title: 'ID', minWidth:80, align: "center", sort: true, fixed: 'left', templet: function (d) {
                    return d.LAY_INDEX;
                }}
            ,{field: 'jobNumber', title: '工号', align: "center", minWidth: 172, sort: true}
            ,{field: 'username', title: '姓名', align: "center", minWidth: 105}
            ,{field: 'sex', title: '性别', align: "center", width: 83, sort: true, templet: '#userSex'}
            ,{field: 'academy', title: '学院', minWidth: 90, align: "center"}
            ,{field: 'role', title: '角色', minWidth: 130, align: "center", sort: true, templet: '#userStatus'}
            ,{title: '操作', align: "center", fixed: 'right', minWidth: 250, toolbar: '#listToolBar'}
        ]]
        ,request: {
            pageName: 'pageNo'
            ,limitName: 'pageSize'
        }
        ,response: {
            statusCode: 200
        }
        ,parseData: function (res) {
            return {
                "data": res.data.records,//接受网络上传输的数据进行数据处理
                'code': res.status,
                'msg': res.msg,
                "count": res.data.total
            }
        }
        ,limit: 10
        ,limits: [10, 15, 20, 25, 30]
    });

    //每行工具栏监听
    table.on('tool(user-list)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除？',function (index) {
                $.ajax({
                    url: '/user/' + data.id
                    ,type: 'DELETE'
                    ,success: function (data) {
                        if (data.status === 200) {
                            layer.msg(data.msg);
                            obj.del();
                        }else{
                            layer.confirm(data.msg)
                        }
                    }
                });
                layer.close(index);
            })
        }else if (obj.event === 'update') {
            layer.open({
                type: 2
                ,title: '修改用户信息'
                ,content: '/user/edit'
                ,maxmin: true
                ,btn: ['确定', '取消']
                ,area: ['500px', '400px']
                ,success: function (layero, index) {
                    //事件监听
                    var othis = layero.find('iframe').contents().find("#editForm").click();

                    //为表单赋初始值
                    othis.find('input[name="id"]').val(data.id);
                    othis.find('input[name="jobNumber"]').val(data.jobNumber);
                    othis.find('input[name="username"]').val(data.username);
                    othis.find('input[name="academy"]').val(data.academy);

                    if (data.sex == 1 || data.sex == true) {
                        othis.find('input[name="sex"][value="true"]').prop('checked', true);
                        othis.find('input[name="sex"][value="false"]').attr('checked', false);
                    } else {
                        othis.find('input[name="sex"][value="false"]').attr('checked', true);
                        othis.find('input[name="sex"][value="true"]').attr('checked', false);
                    }
                }
                ,yes: function(index, layero){
                    var iframeWindow = window['layui-layer-iframe'+ index]
                        ,submitID = 'user-edit-submit'
                        ,submit = layero.find('iframe').contents().find("#" + submitID);

                    // 监听提交
                    iframeWindow.layui.form.on('submit(' + submitID +')', function(data1){
                        var field = data1.field;

                        //表单验证
                        var jobNumber = field.jobNumber;
                        
                        // if (! /^\d{8}$/.test(jobNumber)){
                        //     layer.alert('工号由8位数字组成', {icon: 2});
                        //     return false;
                        // }

                        $.ajax({
                            url: '/user',
                            type: 'PUT',
                            contentType: "application/json",
                            data: JSON.stringify(field),
                            success: function (res) {
                                if (res.status == 200){
                                    obj.update({
                                        username: field.username
                                        ,jobNumber: field.jobNumber
                                        ,sex: field.sex
                                        ,academy: field.academy
                                    }); // 数据更新
                                    layer.msg(res.msg);
                                    form.render();
                                    // 关闭弹层
                                }else
                                    layer.msg(res.msg);
                                layer.close(index);
                            }
                        });
                    });

                    submit.trigger('click');
                }
            })
        } else if(obj.event === 'asAdmin') {
            $.ajax({
                url: '/user',
                type: 'PUT',
                contentType: "application/json",
                data: JSON.stringify({id: data.id, role: 'admin'}),
                success: function (d) {
                    layer.msg(d.msg);
                    // 重载表格
                    tableIns.reload();
                }
            })
        } else if (obj.event === 'asUser') {
            $.ajax({
                url: '/user',
                type: 'PUT',
                contentType: "application/json",
                data: JSON.stringify({id: data.id, role: 'user'}),
                success: function (d) {
                    layer.msg(d.msg);
                    // 重载表格
                    tableIns.reload();
                }
            })
        }
    });
    //输出接口
    exports('userList', {});
});