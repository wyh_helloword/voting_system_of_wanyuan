function child(obj){
    var id = obj;//获取父界面的传值,id就是对应的vid
    layui.define(['table', 'layer', 'form'], function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
        var $ = layui.$
            , table = layui.table
            , layer = layui.layer
            , form = layui.form;
        var tableIns = table.render({
            elem: '#candidateList'
            ,url: '/candidate/' + id //数据接口
            ,method: 'GET'
            ,page: true //开启分页
            ,toolbar: '#toolBar'
            ,defaultToolbar: ['filter', 'print']
            ,cols: [[ //表头
                {field: 'id', title: 'ID', minWidth:80, align: "center", sort: true, fixed: 'left', templet: function (d) {
                        return d.LAY_INDEX;
                    }}
                ,{field: 'jobNumber', title: '工号', align: "center", minWidth: 172, sort: true, templet: function (d) {
                        return d.user.jobNumber;
                    }}
                ,{field: 'username', title: '姓名', align: "center", minWidth: 105, templet: function (d) {
                        return d.user.username;
                    }}
                ,{field: 'academy', title: '学院', minWidth: 90, align: "center", templet: function (d) {
                        return d.user.academy;
                    }}
                ,{field: 'sex', title: '性别', align: "center", minWidth: 70, sort: true, templet: '#userSex'}
                ,{field: 'rank', title: '职位', align: "center", minWidth: 70, sort: true}
                // ,{field: 'video_url', title: '视频', align: "center", width: 120, templet: function (d) {
                //         console.log(d.videoUrl);
                //         if (d.videoUrl == "" || d.videoUrl == null)
                //             return "<a class='layui-btn  layui-btn-warm layui-btn-xs' lay-event='addVideo'>" +
                //                 "        添加" +
                //                 "   </a>"
                //     }}
                // ,{field: 'photo_url', title: '图片', align: "center", width: 120, templet: function (d) {
                //         if (d.photoUrl == "" || d.photoUrl == null)
                //             return "<a class='layui-btn layui-btn-xs' lay-event='addPhoto'>" +
                //                 "        添加" +
                //                 "   </a>"
                //     }}
                ,{field: 'poll', title: '获得票数', align: "center", width: 120, sort: true}
                ,{title: '操作', align: "center", fixed: 'right', minWidth: 250, toolbar: '#listToolBar'}
            ]]
            ,request: {
                pageName: 'pageNo'
                ,limitName: 'pageSize'
            }
            ,response: {
                statusCode: 200
            }
            ,parseData: function (res) {
                return {
                    "data": res.data.records,
                    'code': res.status,
                    'msg': res.msg,
                    "count": res.data.total
                }
            }
            ,limit: 10
            ,limits: [10, 15, 20, 25, 30]
        });
        //监听总工具栏
        $('#add').click(function () {
            layer.open({
                type: 2,
                title: '添加候选人',
                shadeClose: true,
                shade: 0.8,
                area: ['70%', '90%'],
                content: '/candidate/add', //iframe的url
                success: function (layero, index) {
                    var iframe = window['layui-layer-iframe' + index];
                    iframe.getVid(id);
                }
            });
        });
        //每行工具栏监听
        table.on('tool(candidate-list)', function (obj) {
            var data = obj.data;
            if (obj.event === 'del') {
                layer.confirm('确定删除？',function (index) {
                    $.ajax({
                        url: '/candidate/' + data.id
                        ,type: 'DELETE'
                        ,success: function (data) {
                            if (data.status === 200) {
                                layer.msg(data.msg);
                                obj.del();
                            }else{
                                layer.confirm(data.msg)
                            }
                        }
                    });
                    layer.close(index);
                })
            }else if (obj.event === 'update') {
                layer.open({
                    type: 2
                    ,title: '修改候选人资料'
                    ,content: '/candidate/edit'
                    ,maxmin: true
                    ,btn: ['确定', '取消']
                    ,area: ['600px', '500px']
                    ,success: function (layero, index) {
                        var othis = layero.find('iframe').contents().find("#editForm").click();

                        //为表单赋初始值
                        othis.find('textarea[name="details"]').val(data.details);
                        othis.find('input[id="vid"]').val(data.vid);
                        othis.find('input[name="rank"]').val(data.rank);
                        othis.find('input[id="cid"]').val(data.id);
                    }
                    ,yes: function(index, layero){
                        var iframeWindow = window['layui-layer-iframe'+ index]
                            ,submitID = 'candidate-edit-submit'
                            ,submit = layero.find('iframe').contents().find("#" + submitID);

                        // 监听提交
                        iframeWindow.layui.form.on('submit(' + submitID +')', function(data1){
                            var field = data1.field;
                            $.ajax({
                                url: '/candidate',
                                type: 'PUT',
                                contentType: "application/json",
                                data: JSON.stringify({id: data.id, details: field.details, rank: field.rank}),
                                success: function (res) {
                                    if (res.status == 200){
                                        layer.msg(res.msg);
                                        form.render();
                                        obj.update({
                                            rank: field.rank
                                        });
                                        layer.close(index);// 关闭弹层
                                    }else
                                        layer.msg(res.msg);
                                }
                            });
                        });

                        submit.trigger('click');
                    }
                })
            }
        });
        //输出接口
        exports('candidateList', {});
    });
}
