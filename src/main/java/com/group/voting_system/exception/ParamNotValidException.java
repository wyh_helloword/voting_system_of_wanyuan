package com.group.voting_system.exception;

/**
 * @Author: xyf
 * @Date: 2019/3/26 12:03
 */
public class ParamNotValidException extends Exception {
    public ParamNotValidException(String msg){
        super(msg);
    }

    public ParamNotValidException(){
        super("请求参数无效！");
    }
}
