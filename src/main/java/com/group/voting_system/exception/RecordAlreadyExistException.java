package com.group.voting_system.exception;

/**
 * @Author: xyf
 * @Date: 2019/3/26 12:05
 */
public class RecordAlreadyExistException extends Exception {
    public RecordAlreadyExistException(String msg){
        super(msg);
    }

    public RecordAlreadyExistException(){
        super("记录已存在！");
    }
}
