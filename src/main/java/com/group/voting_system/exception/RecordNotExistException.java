package com.group.voting_system.exception;

/**
 * @Author: xyf
 * @Date: 2019/3/26 12:49
 */
public class RecordNotExistException extends Exception {
    public RecordNotExistException(String msg){
        super(msg);
    }
    public RecordNotExistException(){
        super("记录不存在！");
    }
}
