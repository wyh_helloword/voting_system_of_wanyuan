package com.group.voting_system.exception;

import com.group.voting_system.utils.RestResult;
import com.group.voting_system.utils.ResultUtil;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author: xyf
 * @Date: 2019/3/26 20:31
 */
@RestControllerAdvice
public class Handler {

    @ExceptionHandler(ParamNotValidException.class)
    public RestResult paramNotValid(ParamNotValidException e){
        return ResultUtil.fail(411, e.getMessage());
    }

    @ExceptionHandler(RecordAlreadyExistException.class)
    public RestResult recordAlreadyExist(RecordAlreadyExistException e){
        return ResultUtil.fail(412, e.getMessage());
    }

    @ExceptionHandler(RecordNotExistException.class)
    public RestResult recordNotExist(RecordNotExistException e){
        return ResultUtil.fail(413, e.getMessage());
    }
}
