package com.group.voting_system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User {

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 性别
     */
    private Boolean sex;

    /**
     * 所属学院
     */
    private String academy;

    /**
     * 角色：分为user和admin两种
     */
    private String role;

    /**
     * 工号
     */
    private String jobNumber;

    /**
     * 密码
     */
    private String password;

}
