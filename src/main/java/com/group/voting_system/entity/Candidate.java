package com.group.voting_system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Candidate implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户的id
     */
    private Integer uid;

    @TableField(exist = false)
    private User user;

    /**
     * 投票事件的id
     */
    private Integer vid;

    private Vote vote;

    /**
     * 视频在服务器上的存放地址
     */
    private String videoUrl;

    /**
     * 个人照片在服务器上的地址
     */
    private String photoUrl;

    /**
     * 个人事迹的详细描述
     */
    private String details;

    /**
     * 候选人获得的票数
     */
    private Integer poll;

    /**
     * 候选人的职位
     */
    private String rank;
}
