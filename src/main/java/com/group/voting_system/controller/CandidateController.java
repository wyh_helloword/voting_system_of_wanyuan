package com.group.voting_system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.group.voting_system.entity.Candidate;
import com.group.voting_system.entity.User;
import com.group.voting_system.entity.Vote;
import com.group.voting_system.service.ICandidateService;
import com.group.voting_system.service.IVoteService;
import com.group.voting_system.utils.RestResult;
import com.group.voting_system.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;

/**
 * <p>
 *  逻辑控制器
 *  候选人的增删改查都写在这里
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/candidate")
public class CandidateController {

    @Autowired
    ICandidateService candidateService;

    @Autowired
    IVoteService voteService;

    @GetMapping("/details")
    public String detailsPage(@RequestParam(value = "cid") Integer cid,
                              @RequestParam(value = "vid") Integer vid,
                              Model model){
        Candidate candidate = candidateService.getCandidateByCid(cid);
        List<Candidate> candidates = candidateService.getCandidateByVId(vid);
        //因为每个学院只选出一个教师，那么就要对教师或辅导员进行学院排序
        Set<String> academy = new HashSet<>();
        candidates.forEach(x -> {
            academy.add(x.getUser().getAcademy());
        });
        model.addAttribute("candidate", candidate);
        model.addAttribute("academy", academy);
        return "vote/new_details";
    }

    @GetMapping("/list")
    public String listPage(){
        return "candidate/list";
    }

    @GetMapping("/add")
    public String addPage(){
        return "candidate/add";
    }

    @GetMapping("/edit")
    public String editPage(){
        return "candidate/edit";
    }

    @ResponseBody
    @GetMapping("/{vid}")
    public RestResult list(@PathVariable("vid") Integer vid,
                           @RequestParam(value = "pageSize", defaultValue = "1") Integer pageSize,
                           @RequestParam(value = "pageNo", defaultValue = "10") Integer pageNo){

        IPage<Candidate> candidate = candidateService.getCandidateByVid(
                new Page(pageNo, pageSize), vid);

        return ResultUtil.success("", candidate);
    }

    @ResponseBody
    @DeleteMapping("/{cid}")
    public RestResult del(@PathVariable("cid") Integer cid){
        boolean b = candidateService.removeById(cid);
        if (!b)
            return ResultUtil.fail("删除失败");
        return ResultUtil.success("删除成功");
    }

    @ResponseBody
    @PostMapping("")
    public RestResult add(@RequestBody Candidate candidate){
        boolean b = candidateService.save(candidate);
        if (!b)
            return ResultUtil.fail("添加失败");
        return ResultUtil.success("添加成功");
    }

    @ResponseBody
    @PutMapping("")
    public RestResult update(@RequestBody Candidate candidate){
        boolean b = candidateService.updateById(candidate);
        if (!b)
            return ResultUtil.fail("更新失败");
        return ResultUtil.success("更新成功");
    }

    @GetMapping("/search")
    public String search(@RequestParam("username") String username,
                         @SessionAttribute("user") User user,
                         @RequestParam("vid") Integer vid,
                         Model model){

        List<Candidate> list = candidateService.searchCandidateByUsername(username, vid);
        Vote vote = voteService.getById(vid);

        model.addAttribute("candidates", list);
        model.addAttribute("user", user);
        model.addAttribute("vote", vote);
        model.addAttribute("userCount", voteService.getUserCount(vid));
        model.addAttribute("leftDays", voteService.getLeftDays(vid));
        model.addAttribute("userVotes", voteService.getAllPoll(vid));
        return "vote/vote";
    }

}
