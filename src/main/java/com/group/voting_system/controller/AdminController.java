package com.group.voting_system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: xyf
 * @Date: 2019/4/27 15:02
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    @GetMapping("")
    public String index(){
        return "admin/index";
    }
}
