package com.group.voting_system.controller;

import com.group.voting_system.entity.User;
import com.group.voting_system.exception.RecordNotExistException;
import com.group.voting_system.service.IUserService;
import com.group.voting_system.utils.RestResult;
import com.group.voting_system.utils.ResultUtil;
import com.group.voting_system.utils.SecureUtil;
import com.group.voting_system.utils.VerifyCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * @Author: xyf
 * @Date: 2019/4/15 20:11
 * 登录的统一权限管理
 */
@Controller
public class AuthController {

    @Autowired
    IUserService userService;

//    控制用户投票的登录
    @GetMapping("/user/login")
    public String userLogin(){
        return "login";
    }

    @PostMapping("/user/login")
    @ResponseBody
    public RestResult login(@RequestBody User user, HttpServletRequest request) throws RecordNotExistException {

        User user1 = userService.getUserByJobNumAndPass(user);

        if (user1 != null){
            HttpSession session = request.getSession();
            session.setAttribute("user", user1);
            String url = (String) session.getAttribute("voteUrl");
            if (url == null)
                return ResultUtil.success("登录成功，请重新扫码");
            else{
                session.removeAttribute("voteUrl");
                return ResultUtil.success("登录成功", url);
            }

        }

        return ResultUtil.fail("用户名或密码错误");
    }

    @GetMapping("/user/logout")
    public String logout(HttpSession session){
        session.removeAttribute("user");
        return "redirect:login";
    }

    @GetMapping("/changePass")
    public String changePass(){
        return "changePass";
    }

    @ResponseBody
    @PostMapping("/changePass")
    public RestResult change(@RequestBody Map<String, String> map) throws RecordNotExistException {
        User user = userService.getUserByJobNumAndPass(new User().setJobNumber(map.get("jobNumber"))
                .setPassword(map.get("password")));
        if (user == null){
            throw new RecordNotExistException("账号或密码错误");
        }
        boolean b = userService.updateById(user.setPassword(SecureUtil.encryptBasedDes(map.get("newPass"))));
        return  b ? ResultUtil.success("修改成功,请重新访问") : ResultUtil.fail("修改失败");
    }

    //请求二维码
    @GetMapping("/code")
    public void code(HttpSession session,HttpServletResponse response){
        VerifyCode verifyCode = new VerifyCode();
        BufferedImage image = verifyCode.getImage();
        String text = verifyCode.getText();
        session.setAttribute("code", text);
        try {
            VerifyCode.output(image, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
