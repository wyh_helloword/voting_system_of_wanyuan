package com.group.voting_system.utils;

/**
 * @Author: xyf
 * @Date: 2019/3/26 11:57
 */
public class ResultUtil {
    public static RestResult success(String msg, Object data){
        RestResult restResult = new RestResult();
        restResult.setStatus(200);
        restResult.setMsg(msg);
        restResult.setData(data);

        return restResult;
    }
    public static RestResult success(String msg){
        RestResult restResult = new RestResult();
        restResult.setStatus(200);
        restResult.setMsg(msg);

        return restResult;
    }
    public static RestResult fail(Integer status, String msg){
        RestResult result = new RestResult();
        result.setStatus(status);
        result.setMsg(msg);

        return result;
    }
    public static RestResult fail(String msg){
        RestResult result = new RestResult();
        result.setStatus(411);
        result.setMsg(msg);

        return result;
    }
}
