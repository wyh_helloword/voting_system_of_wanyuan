package com.group.voting_system.utils;

import com.group.voting_system.entity.User;
import com.group.voting_system.exception.ParamNotValidException;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xyf
 * @Date: 2019/5/4 23:05
 */
public class ExcelUtil {

    //是否符合2003Excel
    public static boolean isExcel2003(String filePath){
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    //是否符合2007Excel
    public static boolean isExcel2007(String filePath){
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }

    public static List<User> readExcel(MultipartFile file){
        Workbook workbook = null;
        List<User> list = new ArrayList<>();
        if (ExcelUtil.isExcel2007(file.getOriginalFilename())){
            try {
                workbook = new XSSFWorkbook(file.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (ExcelUtil.isExcel2003(file.getOriginalFilename())){
            try {
                workbook = new HSSFWorkbook(file.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Sheet sheet = workbook.getSheetAt(0);
//        从第一个数据开始读
        for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getLastRowNum(); i++){
            Row row = sheet.getRow(i);
            //获取工号
            Cell jobNumberCell = row.getCell(0);
            jobNumberCell.setCellType(1);
            String jobNumber = jobNumberCell.getStringCellValue();
            //获取姓名
            String username = row.getCell(1).getStringCellValue();
            //获取性别
            String sex = row.getCell(2).getStringCellValue();
            //获取学院
            String academy = row.getCell(3).getStringCellValue();
            //获取密码
            String password = "";
            try {
                password = row.getCell(4).getStringCellValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (jobNumber.equals("") || username == null){
                break;
            }
            User user = new User();
            user.setUsername(username);
            user.setJobNumber(jobNumber);
            user.setSex(sex.equals("男"));
            user.setAcademy(academy);
            user.setPassword(password.substring(password.length()-6));
            list.add(user);

        }

        //最后要关闭流
        try {
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    public static void writeExcel(){
        HSSFWorkbook workbook = new HSSFWorkbook();
        DocumentSummaryInformation info = workbook.getDocumentSummaryInformation();
        info.setCategory("育人贡献奖投票统计");
        HSSFSheet sheet = workbook.createSheet("投票统计");
        HSSFRow header = sheet.createRow(0);
        HSSFCell cell0 = header.createCell(0);
        HSSFCell cell1 = header.createCell(1);
        HSSFCell cell2 = header.createCell(2);
        HSSFCell cell3 = header.createCell(3);
        HSSFCell cell4 = header.createCell(4);
        cell0.setCellValue("工号");
        cell1.setCellValue("姓名");
        cell2.setCellValue("学院");
        cell3.setCellValue("学生投票数");
        cell4.setCellValue("教师投票数");
    }
}
