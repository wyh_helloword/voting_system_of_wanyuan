package com.group.voting_system.utils;

import org.apache.poi.xwpf.converter.core.BasicURIResolver;
import org.apache.poi.xwpf.converter.core.FileImageExtractor;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @Author: xyf
 * @Date: 2019/6/18 20:09
 */
public class WordUtil {
    public static void word2007ToHtml(InputStream source, File target, File img) throws IOException {
        OutputStreamWriter writer = null;
        XWPFDocument document = new XWPFDocument(source);
        XHTMLOptions options = XHTMLOptions.create();
        //存放图片文件夹
        options.setExtractor(new FileImageExtractor(img));
        //html中的图片路径
        options.URIResolver(new BasicURIResolver("image"));

        writer = new OutputStreamWriter(new FileOutputStream(target), StandardCharsets.UTF_8);
        XHTMLConverter converter = (XHTMLConverter) XHTMLConverter.getInstance();
        converter.convert(document, writer, options);
    }
}
