package com.group.voting_system.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @Author: xyf
 * @Date: 2019/6/25 22:19
 */
public class VideoThread extends Thread {
    private Process p;
    public VideoThread(Process p){
        this.p = p;
    }

    @Override
    public void run(){
        BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        String line = null;
        try {
            while ((line = err.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                err.close();
                try {
                    p.waitFor();
                    p.destroy();
                    System.out.println("video convert completed...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
