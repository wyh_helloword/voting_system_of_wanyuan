package com.group.voting_system.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

/**
 * @Author: xyf
 * @Date: 2018/12/9 20:59
 */
public class VerifyCode {

    //宽度
    private int w = 120;
    //高度
    private int h = 40;
    //生成随机数
    private Random random = new Random();
    //可选字体
    private String[] frontNames = {"宋体", "华文楷体", "黑体", "微软雅黑", "楷体_GB2312"};
    //可选的字符
    private String codes  = "0123456789abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ";
    //背景色
    private Color bgcolor = new Color(255,255,255);
    //验证码上的文本
    private String text;

    //生成随机的颜色
    private Color ColorRandom(){
        int red     = random.nextInt(150);
        int green   = random.nextInt(150);
        int blue    = random.nextInt(150);
        return new Color(red, green, blue);
    }

    //生成随机字体
    private Font FontRandom(){
        int index = random.nextInt(frontNames.length);
        String fontName = frontNames[index];
        //生成的字体的样式
        //0（无样式）1（粗体）2（斜体）3（粗体+斜体）
        int style = random.nextInt(4);
        //字号
        int size  = random.nextInt(5) + 24;
        return new Font(fontName, style, size);
    }

    //画干扰线
    private void drawLine(BufferedImage image){
        int num = 5;
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        for (int i = 0; i < num; i++){
            int x1 = random.nextInt(w);
            int y1 = random.nextInt(h);
            int x2 = random.nextInt(w);
            int y2 = random.nextInt(h);
            //定义线条特征
            g2.setStroke(new BasicStroke(1.5F));
            g2.setColor(Color.BLUE);
            g2.drawLine(x1, y1, x2, y2);
        }
    }

    //随机生成字符
    private char charRandom(){
        int index = random.nextInt(codes.length());
        return codes.charAt(index);
    }

    //创建BufferedImage
    private BufferedImage createImage(){
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        g2.setColor(this.bgcolor);
        g2.fillRect(0, 0, w, h);
        return image;
    }

    //调用方法得到验证码
    public BufferedImage getImage(){
        BufferedImage image = createImage();
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        StringBuilder sb = new StringBuilder();
        //向图片中画4个字符
        for(int i = 0; i < 4; i++){
            String s = charRandom() + "";
            sb.append(s);
            float x = i * 1.0F * w / 4;
            g2.setFont(FontRandom());
            g2.setColor(ColorRandom());
            g2.drawString(s, x, h-5);
        }

        this.text = sb.toString();
        drawLine(image);
        return image;
    }

    //返回验证码图片上的文本
    public String getText(){
        return text;
    }

    //保存图片到指定的输出流
    public static void output(BufferedImage image, OutputStream out) throws IOException {
        ImageIO.write(image, "JPEG", out);
    }

}
