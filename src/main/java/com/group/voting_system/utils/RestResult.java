package com.group.voting_system.utils;

import lombok.Data;

/**
 * @Author: xyf
 * @Date: 2019/3/26 11:54
 */
@Data
public class RestResult {
    private Integer status = 403;
    private String msg = "";
    private Object data = "";
}
