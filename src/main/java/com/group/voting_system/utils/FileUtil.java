package com.group.voting_system.utils;

import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Author: xyf
 * @Date: 2019/5/12 21:37
 */
public class FileUtil {

    public static String detectFileType(InputStream stream) throws IOException {
        Detector detector = new DefaultDetector();
        Metadata metadata = new Metadata();

        MediaType mediaType = detector.detect(stream, metadata);
        return mediaType.toString();
    }

    public static void processMedia(String source, String targetPath){
        VideoConvert convert = new VideoConvert("G:\\Code\\ffmpeg\\bin\\ffmpeg.exe");
        File target = new File(targetPath);
        if (!target.exists())
            target.mkdirs();
        try {
            convert.start(source, targetPath + "/video.m3u8");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
