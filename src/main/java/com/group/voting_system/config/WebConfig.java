package com.group.voting_system.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @Author: xyf
 * @Date: 2019/3/25 21:53
 */
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    @Value("${localVideo.m3u8}")
    String m3u8Path;

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/video/**").addResourceLocations("file:" + m3u8Path + "/");
    }

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {

        String[] baseUrl = {"/static/**", "/user/login", "/error", "/changePass"};

        registry.addInterceptor(new AdminInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(baseUrl)
                .excludePathPatterns("/user/vote/**", "/vote/file/**", "/user/vote2candidate/**", "/candidate/details/**", "/candidate/search")
                .excludePathPatterns("/pic/**", "/video/**", "/detail/**", "/code");
    }

//    @Bean(name = "multipartResolver")
////    public MultipartConfigElement multipartResolver(){
////        MultipartConfigFactory factory = new MultipartConfigFactory();
////        factory.setMaxFileSize("100MB");
////        return factory;
////    }
}
