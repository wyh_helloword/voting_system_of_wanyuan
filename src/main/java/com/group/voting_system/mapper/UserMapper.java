package com.group.voting_system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.group.voting_system.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.group.voting_system.entity.Vote;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
public interface UserMapper extends BaseMapper<User> {

    User getUserById(@Param("id") Integer id);

    /**
     * 查询每个用户的投票数量
     * @param vid
     * @param uid
     * @return
     */
    Integer countUserVotes(@Param("vid") Integer vid,
                             @Param("uid") Integer uid);


    /**
     * 查询每个用户对每个候选人的投票情况
     * @param vid
     * @param uid
     * @param cid
     * @return
     */
    Integer countCandidateVotes(@Param("vid") Integer vid,
                                  @Param("uid") Integer uid,
                                  @Param("cid") Integer cid);

    /**
     * 在规定的时间内进行投票
     * @param vote
     * @param uid
     * @param c
     * @param time
     * @return
     */
    Boolean vote(@Param("vote") Vote vote,
                 @Param("uid") Integer uid,
                 @Param("c") Set<Integer> c,
                 @Param("time")Date time,
                 @Param("ip") String ip);

    /**
     * 分页查询
     */
    IPage<User> getAllUsers(Page<User> page);

    IPage<User> pageUserNotCandidate(Page<User> page, @Param("vid") Integer vid, @Param("condition") User condition);

    //查询该用户一共投了几票给辅导员
    Integer countAssisVotes(Integer vid, Integer uid);

    //查询该用户一共投了几票给教师
    Integer countTeacherVotes(Integer vid, Integer uid);
}
