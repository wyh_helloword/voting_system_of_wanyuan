package com.group.voting_system.mapper;

import com.group.voting_system.entity.Vote;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
public interface VoteMapper extends BaseMapper<Vote> {
    Vote getVoteById(@Param("id") Integer id);
    Integer addVote(Vote vote);
    Integer getAllPoll(@Param("vid") Integer vid);
    List<Integer> getAllCid(@Param("vid") Integer vid);
    Integer getUserCount(@Param("vid") Integer vid);

    //获取投票次数为两次的ip
    List<String> getIllegalIPs();
}
