package com.group.voting_system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.group.voting_system.entity.Candidate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
public interface CandidateMapper extends BaseMapper<Candidate> {
    List<Candidate> getCandidateByVid(@Param("vid") Integer vid);
    Candidate getCandidateByCid(@Param("cid") Integer cid);

    IPage<Candidate> getCandidateByPoll(Page page, @Param("vid") Integer vid);

    Integer getVotePoll(@Param("cid") Integer cid, @Param("vid") Integer vid);

    Candidate getCandidateById(@Param("id") Integer id);

    boolean isCandidate(@Param("uid") Integer uid, @Param("vid") Integer vid);

    boolean reviseUrlById(@Param("candidate") Candidate candidate);

    List<Candidate> getCandidateByUsername(@Param("username") String username, @Param("vid") Integer vid);

    Integer getTeacherVotes(@Param("vid") Integer vid, @Param("cid") Integer cid);

    Integer getStudentVotes(@Param("vid") Integer vid, @Param("cid") Integer cid);
}
