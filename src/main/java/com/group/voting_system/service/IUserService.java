package com.group.voting_system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.group.voting_system.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.group.voting_system.exception.ParamNotValidException;
import com.group.voting_system.exception.RecordNotExistException;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
public interface IUserService extends IService<User> {
    User getUserByJobNumAndPass(User user) throws RecordNotExistException;

    boolean userVote(Integer vid, Integer uid, Set<Integer> c, String ip) throws RecordNotExistException, ParamNotValidException;

    IPage<User> selectAllUsers(Page<User> page);

    IPage<User> pageUserNotCandidate(Page<User> page, Integer vid, User condition);
}
