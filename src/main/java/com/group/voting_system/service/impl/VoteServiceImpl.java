package com.group.voting_system.service.impl;

import com.group.voting_system.entity.Candidate;
import com.group.voting_system.entity.Vote;
import com.group.voting_system.mapper.CandidateMapper;
import com.group.voting_system.mapper.VoteMapper;
import com.group.voting_system.service.IVoteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.group.voting_system.utils.DateUtil;
import com.group.voting_system.utils.QRUtil;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
@Service
public class VoteServiceImpl extends ServiceImpl<VoteMapper, Vote> implements IVoteService {

    @Resource
    VoteMapper voteMapper;

    @Resource
    CandidateMapper candidateMapper;

    @Value("${localQR.path}")
    String path;

    @Value("${localQR.logo}")
    String logo;

    @Value("${localExcel.download}")
    String excelPath;

    @Override
    public Boolean addOneVote(Vote vote, HttpServletRequest request) {
        //将设置时间放入
        vote.setSetTime(DateUtil.getNowDate());
        int insert = voteMapper.addVote(vote);
        if (insert == 1){
            //创建二维码
            String content = request.getScheme()+"://"
                    + request.getServerName() +":"
                    + request.getServerPort() + "/user/vote/"
                    + vote.getId();
            QRUtil.createCode(vote.getId().toString(), content, path, 500, logo);
        }
        return insert == 1;
    }

    @Override
    public Integer getAllPoll(Integer vid) {
        Integer poll = voteMapper.getAllPoll(vid);
        return poll;
    }

    @Override
    public Integer getUserCount(Integer vid) {
        Integer userCount = voteMapper.getUserCount(vid);
        return userCount;
    }

    @Override
    public Integer getLeftDays(Integer vid) {
        Vote vote = voteMapper.selectById(vid);
        Integer leftDays = Math.toIntExact((vote.getEndTime().getTime() - vote.getStartTime().getTime()) / (1000 * 24 * 3600));
        return leftDays;
    }

    @Override
    public void downloadStatistics(Integer vid) {

        List<Integer> list = voteMapper.getAllCid(vid);

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("投票统计");
        HSSFRow header = sheet.createRow(0);
        HSSFCell cell0 = header.createCell(0);
        HSSFCell cell1 = header.createCell(1);
        HSSFCell cell2 = header.createCell(2);
        HSSFCell cell3 = header.createCell(3);
        HSSFCell cell4 = header.createCell(4);
        HSSFCell cell5 = header.createCell(5);
        cell0.setCellValue("工号");
        cell1.setCellValue("姓名");
        cell2.setCellValue("职位");
        cell3.setCellValue("学院");
        cell4.setCellValue("学生投票数");
        cell5.setCellValue("教师投票数");

        for (int i = 0; i < list.size(); i++) {

            HSSFRow row = sheet.createRow(i + 1);
            Integer t = candidateMapper.getTeacherVotes(vid, list.get(i));
            Integer s = candidateMapper.getStudentVotes(vid, list.get(i));
            Candidate candidate = candidateMapper.getCandidateById(list.get(i));

            HSSFCell cella = row.createCell(0);
            HSSFCell cellb = row.createCell(1);
            HSSFCell cellc = row.createCell(2);
            HSSFCell celld = row.createCell(3);
            HSSFCell celle = row.createCell(4);
            HSSFCell cellf = row.createCell(5);
            cella.setCellValue(candidate.getUser().getJobNumber());
            cellb.setCellValue(candidate.getUser().getUsername());
            cellc.setCellValue(candidate.getRank());
            celld.setCellValue(candidate.getUser().getAcademy());
            celle.setCellValue(s);
            cellf.setCellValue(t);
        }

        File file = new File(excelPath);
        if (!file.exists())
            file.mkdirs();

        try {
            FileOutputStream out = new FileOutputStream(excelPath + "\\" + vid + ".xls");
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
