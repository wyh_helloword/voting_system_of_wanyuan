package com.group.voting_system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.group.voting_system.entity.Candidate;
import com.group.voting_system.mapper.CandidateMapper;
import com.group.voting_system.service.ICandidateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
@Service
public class CandidateServiceImpl extends ServiceImpl<CandidateMapper, Candidate> implements ICandidateService {

    @Autowired
    CandidateMapper candidateMapper;

    @Override
    public List<Candidate> getCandidateByVId(Integer vid) {
        List<Candidate> candidates = candidateMapper.getCandidateByVid(vid);
        return candidates;
    }

    @Override
    public Candidate getCandidateByCid(Integer cid) {
        Candidate candidate = candidateMapper.getCandidateByCid(cid);
        return candidate;
    }

    @Override
    public IPage<Candidate> getCandidateByVid(Page page, Integer vid) {
        IPage<Candidate> candidate = candidateMapper.getCandidateByPoll(page, vid);
        return candidate;
    }

    @Override
    public Candidate getCandidateById(Integer id) {
        Candidate candidate = candidateMapper.getCandidateById(id);
        return candidate;
    }

    @Override
    public boolean reviseUrlById(Candidate candidate) {
        boolean b = candidateMapper.reviseUrlById(candidate);
        return b;
    }

    @Override
    public List<Candidate> searchCandidateByUsername(String username, Integer vid) {
        List<Candidate> candidate = candidateMapper.getCandidateByUsername(username, vid);
        return candidate;
    }
}
