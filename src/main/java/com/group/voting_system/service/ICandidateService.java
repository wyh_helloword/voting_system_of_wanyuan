package com.group.voting_system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.group.voting_system.entity.Candidate;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
public interface ICandidateService extends IService<Candidate> {
    List<Candidate> getCandidateByVId(Integer vid);
    Candidate getCandidateByCid(Integer cid);
    IPage<Candidate> getCandidateByVid(Page page, Integer vid);
    Candidate getCandidateById(Integer id);

    boolean reviseUrlById(Candidate candidate);

    List<Candidate> searchCandidateByUsername(String username, Integer vid);
}
