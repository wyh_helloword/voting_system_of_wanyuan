package com.group.voting_system.service;

import com.group.voting_system.entity.Vote;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xyf
 * @since 2019-04-15
 */
public interface IVoteService extends IService<Vote> {
    Boolean addOneVote(Vote vote, HttpServletRequest request);

    Integer getAllPoll(Integer vid);

    Integer getUserCount(Integer vid);

    Integer getLeftDays(Integer vid);

    void downloadStatistics(Integer vid);
}
